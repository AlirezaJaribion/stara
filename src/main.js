import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import BootstrapVue from 'bootstrap-vue'
import VuePersianDatetimePicker from 'vue-persian-datetime-picker'
import getImg from '@/mixins/getImg'
import i18n from './i18n'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.mixin(getImg)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDCiUoZAQ3aiQ26MuycMzK6tWlxMyXyMrs',
    libraries: 'places'
  }
})

Vue.use(VuePersianDatetimePicker, {
  name: 'DatePicker',
  props: {
    locale: 'en',
    format: 'YYYY-MM-DD',
    editable: false,
    inputClass: 'form-control',
    color: '#044bc7',
    autoSubmit: false
  }
})

Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  i18n,
  name: 'app',
  render: h => h(App)
}).$mount('#app')
