import Vue from 'vue'
import { firestorePlugin } from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/storage'
import 'firebase/firestore'

Vue.use(firestorePlugin)

var firebaseConfig = {
  apiKey: 'AIzaSyCyRefRUzV93lHQllzcNDX8YbO_FnkUKyA',
  authDomain: 'junction-2019-5bf4c.firebaseapp.com',
  databaseURL: 'https://junction-2019-5bf4c.firebaseio.com',
  projectId: 'junction-2019-5bf4c',
  storageBucket: 'junction-2019-5bf4c.appspot.com',
  messagingSenderId: '709519462314',
  appId: '1:709519462314:web:7f1d608f7b83c6486625d3'
}

firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore()
export const storage = firebase.storage()

export default firebase
